# Contenedor OpenSUSE / Plasma

<img src="plasma.jpg" alt="Captura de Pantalla" height="400" />

Ejecuta un escritorio KDE Plasma en OpenSUSE.

## Modo de uso

### Descarga el proyecto:

    git clone https://gitlab.com/javipc/contenedor-opensuse.git
    cd contenedor-opensuse

### Construye la imagen:

    podman build -t opensuse -f opensuse.imagen

### Inicia el contenedor:

    Xephyr -resizeable -screen 800x600 :1 &
    podman run -it \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -e DISPLAY=:1 \
        --device /dev/snd \
        --name opensuse \
        --rm localhost/opensuse

### Cierra el contenedor:

    podman container stop opensuse


## Requisitos

* Xephyr: Para la pantalla.
* podman: Para gestionar el contenedor.
* X11: No sé si funciona con Wayland.
* ... Y un par de gigabytes.

## Aplicaciones

* Yast packager
* Dolphin
* Firefox
* Konsole

## Créditos

Basado en [KDE Neon](https://community.kde.org/Neon/Docker) 
Es un proyecto que ejecuta KDE Neon en Docker.

## Colabora

Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



